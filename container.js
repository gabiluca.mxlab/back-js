"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * @whatItDoes holds all the components (controllers , services ,...) and instances
 */
var Container = /** @class */ (function () {
    function Container() {
    }
    /**
     * get an instance of the component
     */
    Container.get = function (token) {
        var _this = this;
        var tokenName;
        if (typeof token !== 'string') {
            tokenName = token['name'];
        }
        else {
            tokenName = token;
        }
        if (this.instances[tokenName]) {
            return this.instances[tokenName];
        }
        var injectable = this.components[tokenName];
        if (injectable) {
            var _dependencies = injectable.dependencies;
            var _constructor = injectable._constructor;
            if (_dependencies.length > 0) {
                var dependecies = _dependencies.map(function (dependency) { return _this.get(dependency); });
                return this.instances[tokenName] = new (_constructor.bind.apply(_constructor, [void 0].concat(dependecies)))();
            }
            return this.instances[tokenName] = new _constructor();
        }
        return undefined;
    };
    /**
     * Array of components , each component holds its function constructor
     * and its dependencies
     */
    Container.components = [];
    /**
     * see ../handlers/controllerHandler.ts
     */
    Container.controllerHandlers = [];
    /**
     * each component (service ,controller,...) instansiated will be pushed here
     */
    Container.instances = [];
    return Container;
}());
exports.Container = Container;
