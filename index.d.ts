import express = require('express');
import { Container } from './container';
export { Request, Response } from './_http/http';
export { Service, Controller, Get, Post, Put, Delete, Route, RequestBody, ResponseBody } from './decorators/decorators';
export declare class Back {
    static express: typeof express;
    static Container: typeof Container;
    static prepare(app: any): void;
    static reset(): void;
}
